from django.contrib.auth.models import User
from django.db import models
from django.db.models import Avg, Count
import datetime

from django.urls import reverse
from mutagen.mp4 import MP4
from moviepy.video.io.VideoFileClip import VideoFileClip
from django.db.models.signals import pre_save
from django.template.defaultfilters import slugify


class DateTimeModel(models.Model):
    """ A base model with created and edited datetime fields """

    created = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Course(DateTimeModel):
    """ course details """

    languages = (
        ('1', 'English'),
        ('2', 'Hindi')
    )

    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, null=False)
    featured_image = models.FileField(upload_to='images')
    author = models.ForeignKey(User, related_name='courses', on_delete=models.CASCADE)
    author_designation = models.CharField(max_length=50)
    stats = models.CharField(max_length=200)
    description = models.TextField(max_length=1000)
    language = models.CharField(max_length=100, choices=languages)
    rating = models.DecimalField(max_digits=3, blank=True, default=0.0, decimal_places=1)
    price = models.IntegerField()
    what_you_will_learn = models.TextField(max_length=1000)
    requirements = models.CharField(max_length=500)
    related_course = models.ManyToManyField('Course', related_name='courses', blank=True)
    is_active = models.BooleanField(default=True)
    learners = models.ManyToManyField(User, related_name='learners_course', blank=True)

    def slug_save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        return super().save(*args, **kwargs)

    def average_rating(self):
        star = Rating.objects.filter(course=self).count()
        if star > 0:
            reviews = Rating.objects.filter(course=self).aggregate(Avg("rating"))
            avg_rating = Course.objects.filter(title=self).update(rating=reviews['rating__avg'])
            return avg_rating
        else:
            return self.rating

    def save(self, *args, **kwargs):
        self.rating = self.average_rating()
        super(Course, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("module", kwargs={"slug": self.slug})

    def __str__(self):
        return self.title


class Rating(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='ratings')
    rating = models.IntegerField(blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = 'Rating'


class Module(DateTimeModel):
    """ Module details """

    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, null=False)
    duration = models.TimeField(max_length=50, blank=True)
    prerequisite = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    course = models.ForeignKey(Course, related_name='modules', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("module", kwargs={"slug": self.slug})

    class Meta:
        verbose_name_plural = 'Module'


class Lesson(DateTimeModel):
    """ Lesson details """

    name = models.CharField(max_length=100)
    featured_image = models.FileField(upload_to='images')
    description = models.TextField(max_length=500)
    video = models.FileField(upload_to='videos')
    file = models.FileField(upload_to='files')
    duration = models.CharField(max_length=20, blank=True)
    transcript = models.TextField(max_length=1000)
    prerequisite = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    module = models.ForeignKey(Module, related_name='lessons', on_delete=models.CASCADE)

    # def getDuration(self):
    #     clip = VideoFileClip(self.video.path)
    #     duration = clip.duration
    #     video_time = str(datetime.timedelta(seconds=int(duration)))
    #     return video_time
    #
    # def save(self, *args, **kwargs):
    #     self.duration = self.getDuration()
    #     super(Lesson, self).save(*args, **kwargs)



    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Lesson'


def get_duration(sender, instance, **kwargs):
    video = MP4(instance.video)
    duration = video.info.length
    video_time = str(datetime.timedelta(seconds=int(duration)))
    instance.duration = video_time


pre_save.connect(get_duration, sender=Lesson)


class Question(DateTimeModel):
    """ Question details """

    questions = models.CharField(max_length=200)
    mark = models.IntegerField()
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE, related_name='questions')
    question_choices = (
        ('1', 'Multiple Choice'),
        ('2', 'Fill In The Blank'),
        ('3', 'True/False'),
    )
    question_type = models.CharField(max_length=20,
                                     choices=question_choices,
                                     default=None)

    def __str__(self):
        return self.questions

    class Meta:
        verbose_name_plural = 'Question'


class Answer(DateTimeModel):
    """ Answer details """

    answer = models.CharField(max_length=200)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.answer

    class Meta:
        verbose_name_plural = 'Answer'


class Quiz(DateTimeModel):
    """ Quiz details """

    questions = models.CharField(max_length=100)
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.questions

    class Meta:
        verbose_name_plural = 'Quiz'


class QuestionMC(Question):
    """for multiple choice questions"""
    option1 = models.CharField(max_length=100)
    option2 = models.CharField(max_length=100)
    option3 = models.CharField(max_length=100)
    option4 = models.CharField(max_length=100)
    answer = models.CharField(max_length=100)


class QuestionFB(Question):
    """for fill-in-the-blank questions"""
    answer = models.CharField(max_length=100)


class QuestionTF(Question):
    """for true/false questions"""
    answer = models.CharField(max_length=100)


class CourseEnroll(DateTimeModel):
    username = models.CharField(max_length=50)
    course = models.CharField(max_length=50)

    def __str__(self):
        return self.course
