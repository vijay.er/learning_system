from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms

from lmssystem.models import Course, Lesson


class RegisterForm(UserCreationForm):
    email = forms.EmailField(label="Email")

    class Meta:
        model = User
        fields = ('username', 'email',)


class LoginForm(forms.Form):
    email = forms.EmailField(label="email")
    password = forms.CharField(max_length=20,
                               widget=forms.PasswordInput(attrs={'type': 'password', 'name': 'password'}),
                               label="password")

    class Meta:
        model = User
        fields = ("email", 'password')


class CourseEnrollForm(forms.Form):
    course = forms.CharField(max_length=50, error_messages={
        'required': "Please Enter valid course Name"
    })


class EditCourseForm(forms.Form):
    title = forms.CharField(max_length=50)
    description = forms.CharField(max_length=500)
    price = forms.IntegerField()
    what_you_will_learn = forms.CharField(max_length=500)
    requirements = forms.CharField(max_length=50)

    class Meta:
        model = Course
        fields = ("title", 'featured_image', "description", 'price', "what_you_will_learn", 'requirements')


class LessonForm(forms.Form):
    class Meta:
        model = Lesson
        fields = ('__all__',)
        exclude = 'duration'
