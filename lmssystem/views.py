from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from lmssystem.forms import RegisterForm, CourseEnrollForm, LoginForm, EditCourseForm
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from lmssystem.models import Course, CourseEnroll, Rating, Module, Lesson


def index(request):
    """ home page with course details"""
    course = Course.objects.filter(is_active=True)
    review = [Course.average_rating(self=i) for i in course]
    modules = [Module.objects.filter(course=i) for i in course]
    return render(request, 'lmssystem/home.html', context={'course': course, 'modules':modules})


def user_register(request):
    """ register for new user"""
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
        else:
            form = RegisterForm()
            return render(request, 'lmssystem/register.html', {'form': form})
    form = RegisterForm()
    return render(request, 'lmssystem/register.html', {'form': form})


def get_user(email):
    try:
        return User.objects.get(email=email.lower())
    except User.DoesNotExist:
        return None


def user_login(request):
    """ for user login """
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            email = request.POST['email']
            password = request.POST['password']
            username = get_user(email)
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('home')
            else:
                messages.info(request, 'Invalid Credentials')
                return render(request, 'lmssystem/login.html', {'form': form})

    form = LoginForm()
    return render(request, 'lmssystem/login.html', {'form': form})


def user_logout(request):
    """ for user logout"""
    logout(request)
    return redirect('login')


def change_password(request):
    """ for password change"""
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('home')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'lmssystem/passwordchange.html', {
        'form': form
    })


def course_enroll(request):
    """ for enroll a student in a particular course"""
    if request.method == 'POST':
        form = CourseEnrollForm(request.POST)
        if form.is_valid():
            username = request.user.username
            courses = request.POST['course']
            course = Course.objects.filter(title=courses)
            # usercourse = CourseEnroll.objects.filter(username=)
            if course:
                data = CourseEnroll.objects.create(
                    username=username,
                    course=courses,
                )
                data.save()
                messages.success(request, 'thanks for enrollment')
                return redirect('home')
        # else:
        #     messages.error(request, "involid choice")
    course = Course.objects.filter(is_active=True)
    return render(request, 'lmssystem/courseenroll.html', {'course': course})


def edit_course(request):
    """ for update the course details"""
    if request.method == 'POST':
        user = Course.objects.filter(author__username=request.user.username)
        form = EditCourseForm(request.POST)
        if form.is_valid() and user:
            title = request.POST['title']
            description = request.POST['description']
            price = request.POST['price']
            what_you_will_learn = request.POST['what_you_will_learn']
            requirements = request.POST['requirements']
            data = user.update(title=title,
                               description=description,
                               price=price,
                               what_you_will_learn=what_you_will_learn,
                               requirements=requirements)
            return redirect('home')
        else:
            messages.error(request, 'you are not a author of this page')
    else:
        messages.error(request, 'you are not a author of this page')
        form = EditCourseForm()
    return render(request, 'lmssystem/editcourse.html', {'form': form})


def module_view(request, slug):
    """ for module view as per the selected course"""
    print(slug)
    try:
        module = Module.objects.filter(course__title=slug)
        return render(request, 'lmssystem/single_module.html', {'module': module})
    except Course.DoesNotExist:
        return None


def lesson_view(request, slug):
    """ for lesson view as per the selected module"""
    print(slug)
    try:
        mod = Lesson.objects.filter(module__name=slug)
        print("hi")
        return render(request, 'lmssystem/uploadlesson.html', {'module': mod})
    except Course.DoesNotExist:
        return None