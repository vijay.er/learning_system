from django.contrib import admin
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import RegisterForm

from lmssystem.models import *


class CourseAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    # exclude = ['rating']


admin.site.register(Course, CourseAdmin)


class ModuleAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Module, ModuleAdmin)
admin.site.register(Lesson)
admin.site.register(Answer)
admin.site.register(Quiz)
admin.site.register(QuestionTF)
admin.site.register(QuestionFB)
admin.site.register(QuestionMC)
admin.site.register(Question)
admin.site.register(CourseEnroll)
admin.site.register(Rating)


# class AnswerInline(admin.TabularInline):
#     model = QuestionFB


# class Answernline(admin.TabularInline):
#     model = Answer
#
#
# class QuestionAdmin(admin.ModelAdmin):
#     inlines = [Answernline]
#
#




