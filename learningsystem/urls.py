from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from lmssystem.views import (index,
                             user_register,
                             user_logout,
                             user_login,
                             change_password,
                             course_enroll,
                             edit_course,
                             module_view,
                             lesson_view
                             )

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='home'),
    path('register/', user_register, name='register'),
    path('logout/', user_logout, name='logout'),
    path('courseenroll/', course_enroll, name='enroll'),
    path('login/', user_login, name='login'),
    path('changepswd/', change_password, name='password'),
    path('editcourse/', edit_course, name='editcourse'),
    path('module/<slug>/', module_view, name='module'),
    path('lesson/<slug>/', lesson_view, name='lesson'),


    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
